#include "Piece.h"

Piece::Piece(Body body, Color color, Height height, Shape shape)
	:m_body(body), m_color(color), m_height(height), m_shape(shape)
{
	static_assert(sizeof(*this) == 1 , "Piece size is not 1");
	sizeof(*this);
}

Piece::Body Piece::GetBody() const
{
	return m_body;
}

Piece::Color Piece::GetColor() const
{
	return m_color;
}

Piece::Height Piece::GetHeight() const
{
	return m_height;
}

Piece::Shape Piece::GetShape() const
{
	return m_shape;
}

std::ostream& operator<<(std::ostream& out, const Piece& piece)
{
	//resharper
	//out << static_cast<int16_t>(piece.m_body);
	switch (piece.m_body)
	{
	case Piece::Body::Full:
	{
		out << "Full";
		break;
	}
	case Piece::Body::Hallow:
	{
		out << "Hallow";
		break;
	}
	default:
	{
		throw "Invalid body type !";
	}
	}
}
